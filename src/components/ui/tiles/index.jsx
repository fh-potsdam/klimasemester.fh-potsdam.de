import { graphql, StaticQuery } from "gatsby";
import React, { Component } from "react";
import PropTypes from "prop-types";
import Masonry from "react-masonry-css";

import ProjectTile from "./project-tile";
import SocialTile from "./social-tile";
import Styles from "./index.module.scss";


export default class Tiles extends Component {
	static propTypes = {
		onTagClick: PropTypes.func
	};

	static defaultProps = {};

	static BREAKPOINTS = {
		default: 3,
		1100: 3,
		700: 2,
		500: 1,
	};

	renderTile (tileData) {
		return tileData.teaserImg ?
			<ProjectTile
				key={ tileData.teaserImg.asset.assetId }
				onTagClick={ this.props.onTagClick }
				{ ...tileData }
			/>:
			<SocialTile
				key={ tileData.url }
				{ ...tileData }
			/>;
	}

	filterTags ({ tags = [] }) {
		for (const tag of this.props.filter) {
			if (tags.indexOf(tag) !== -1) {
				return true;
			}
		}

		return false;
	}

	renderContent (projects) {
		const filteredProjects =
			!this.props.filter.length ?
				projects :
				projects.filter(this.filterTags.bind(this));

		return (
			<Masonry
				columnClassName={ Styles.column }
				className={ Styles.tiles }
				breakpointCols={ Tiles.BREAKPOINTS }>
				{ filteredProjects.map(tile => this.renderTile(tile)) }
			</Masonry>
		);
	}

	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
						sanity {
							allProjects(limit: 1) {
								specialTags 
								
								projectList {
									... on Sanity_Project {
										headline
										subline
										teacher
										tags
										contentRaw
										
										teaserImg {
											asset {
												assetId
											}
										}
									}
									
									... on Sanity_SocialTile {
										url
									}
								}
							}
						}
					}
				` }
				render={ data => this.renderContent(data.sanity.allProjects[0].projectList) }
			/>
		);
	}
}
