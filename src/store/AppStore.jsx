import { observable, action } from "mobx";


class Store {
	@observable activeTags = [];

	@action updateActiveTags = tags => {
		this.activeTags = tags;
	};
}

export default new Store();