import React from "react";
import TagComp from "react-bulma-components/lib/components/tag/tag";

import Styles from "./index.module.scss";


const Tag = ({ tag, isSpecial, isActive, onToggle }) =>
	<TagComp
		rounded
		data-id={ tag }
		onClick={ onToggle ? () => onToggle(tag) : undefined }
		onKeyDown={ onToggle ? () => onToggle(tag) : undefined }
		className={ [
			Styles.tag,
			isSpecial ? Styles.special : undefined,
			isActive ? Styles.active : undefined
		].join(" ") }>
		# { tag.toLowerCase() }
	</TagComp>;

export default Tag;