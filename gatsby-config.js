const FileSystem = require("fs");
const Path = require("path");

if (FileSystem.existsSync(Path.resolve(__dirname, `.env.${ process.env.NODE_ENV }`))) {
	require("dotenv").config({
		path: `.env.${ process.env.NODE_ENV }`,
	});

	console.log(`Found Environment File .env.${ process.env.NODE_ENV }`);
} else {
	console.log(`Couldn’t find Environment File. Check for other variables …`);
}

module.exports = {
	plugins: [
		`gatsby-plugin-react-helmet`,
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-zopfli`,
			options: {
				extensions: ["css", "html", "js", "svg"],
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `assets`,
				path: `${ __dirname }/src/`,
			},
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `Fachhochschule Potsdam`,
				short_name: `#KLIMA`,
				start_url: `/`,
				background_color: `#FFFFFF`,
				theme_color: `#000000`,
				display: `minimal-ui`,
				icon: `src/assets/favicon/favicon.png`,
			},
		},
		{
			resolve: "gatsby-source-graphql",
			options: {
				// Arbitrary name for the remote schema Query type
				typeName: "Sanity",
				// Field under which the remote schema will be accessible. You'll use this in your Gatsby query
				fieldName: "sanity",
				// Url to query from
				url: "https://8glljqk3.api.sanity.io/v1/graphql/production/default",
			},
		},
		`gatsby-plugin-sass`,
		`gatsby-plugin-offline`
	],
};
