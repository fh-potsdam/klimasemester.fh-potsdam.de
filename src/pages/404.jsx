import React, { Component } from "react";

import Page from "../components/layout/page";


export default class ErrorPage extends Component {
	static propTypes = {};

	static defaultProps = {};

	render () {
		return (
			<Page title={ "404 · Inhalt nicht gefunden" } description={ "404 – Inhalt nicht gefunden" }>
				<div>Der Inhalt konnte nicht gefunden werden. <b>Status: 404</b></div>
			</Page>
		);
	}
}