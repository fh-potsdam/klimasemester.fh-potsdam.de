import { graphql, StaticQuery } from "gatsby";
import React, { Component } from "react";
import PropTypes from "prop-types";

import TagGroup from "react-bulma-components/lib/components/tag/components/tag-group";
import Tag from "../../ui/tag";

import Styles from "./toolbar.module.scss";


export default class Toolbar extends Component {
	static propTypes = {
		onSelectionChanged: PropTypes.func.isRequired
	};

	static defaultProps = {};

	state = {
		selectedTags: []
	};


	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
						sanity {
			                allProjects(limit: 1) {
		                        specialTags
		                        
		                        projectList {
									... on Sanity_Project {
										tags
									}
									... on Sanity_SocialTile {
										tags
									}
								}
			                }
			            }
		            }
				` }
				render={ data => this.renderContent(data) }
			/>
		)
	}

	resetSelectedTags () {
		this.props.onSelectionChanged([]);

		this.setState({
			...this.state,
			selectedTags: []
		});
	}

	renderContent (data) {
		const {
			specialTags,
			projectList
		} = data.sanity.allProjects[0];
		let allProjectTags = [];


		for (const { tags = [] } of projectList) {
			allProjectTags = allProjectTags.concat(tags);
		}

		return (
			<div className={ Styles.toolbar }>
				<TagGroup className={ Styles.tags }>
					{ specialTags.map((tag, index) => this.renderTag(tag, true, index))  }
					{ allProjectTags.map((tag, index) => this.renderTag(tag, false, index)) }
				</TagGroup>

				<p className={ [
						Styles.resetButton,
						this.state.selectedTags.length ? Styles.active : undefined
					].join(" ") }>

					<span
						role="button"
						tabIndex={ 0 }
						onKeyDown={ this.resetSelectedTags.bind(this) }
						onClick={ this.resetSelectedTags.bind(this) }>
						Auswahl löschen
					</span>

				</p>
			</div>
		);
	}

	toggleTag (tag) {
		let { selectedTags } = this.state;

		// remove current tag
		if (selectedTags.indexOf(tag) !== -1) {
			selectedTags = selectedTags.filter(_tag => tag !== _tag);
		} else { // add new tag
			selectedTags.push(tag);
		}

		this.props.onSelectionChanged(selectedTags);

		this.setState({
			...this.state,
			selectedTags
		});
	}

	renderTag (tag, isSpecial, index) {
		const isActive = this.state.selectedTags.indexOf(tag) !== -1;

		return (
			<Tag
				key={ tag }
				tag={ tag }
				isActive={ isActive }
				isSpecial={ isSpecial }
				onToggle={ () => this.toggleTag(tag) }
			/>
		);
	}
}