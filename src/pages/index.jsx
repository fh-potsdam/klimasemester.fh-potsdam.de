import { graphql } from "gatsby";
import React, { Component } from "react";

import Page from "../components/layout/page";
import Teaser from "../components/sections/teaser";
import Projects from "../components/sections/projects";


export default class Home extends Component {
	static propTypes = {};

	static defaultProps = {};

	render () {
		return (
			<Page { ...this.props.data.sanity.allHome[0] }>
				<Teaser />
				<Projects />
			</Page>
		);
	}
}

export const query = graphql`
    query {
	    sanity {
	        allHome (limit: 1) {
                description
                title
            }
	    }
    }
`;