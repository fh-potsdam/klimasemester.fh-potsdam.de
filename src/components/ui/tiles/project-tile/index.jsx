import React from "react";

import Heading from "react-bulma-components/lib/components/heading/heading";
import Content from "react-bulma-components/lib/components/content/content";
// import TagGroup from "react-bulma-components/lib/components/tag/components/tag-group";
import truncate from "truncate";

import Image from "../../image";
// import Tag from "../../tag";
import Styles from "./index.module.scss";


const ProjectTile = ({
	teaserImg,
	tags,
	headline,
	subline,
	teacher,
	description,
	onTagClick = () => {}
}) =>
	<div className={ Styles.tile }>
		{ teaserImg &&
			<Image
				className={ Styles.teaser }
				id={ teaserImg.asset.assetId }
			/>
		}

		<div className={ Styles.content }>
			<Heading
				renderAs={ "h4" }
				textSize={ 4 }
				className={ Styles.headingWrapper }>

				<span className={ Styles.headingSpan }>
					{ truncate(headline, 60) }
				</span>

				<span className={ Styles.sublineSpan }>
					{ truncate(subline, 60) }
				</span>

			</Heading>

			{ description &&
				<Content>
					{ description }
				</Content>
			}

			<p className={ Styles.teacher }>
				{ teacher.join(", ") }
			</p>

			<p className={ Styles.tags }>
				{ tags.map(tag =>
					<span
						key={ tag }
						onClick={ () => onTagClick(tag) }
						onKeyDown={ () => onTagClick(tag) }
						role={ "button" }
						tabIndex={ 0 }
						className={ Styles.tag }>
						# { tag }
					</span>
				) }
			</p>

			{ /* <TagGroup renderAs={ "p" }>
				{ tags.map(tag =>
					<Tag
						key={ tag }
						tag={ tag }
					/>
				) }
			</TagGroup> */ }
		</div>

	</div>;

export default ProjectTile;