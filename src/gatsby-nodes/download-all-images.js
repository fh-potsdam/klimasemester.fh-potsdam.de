const Path = require("path");
const FileSystem = require("fs");
const mkdirp = require("mkdirp");

const downloadImage = require("./download-image.js");


const downloadAllImages = async ({ graphql }) => {
	const
		downloadDir = process.env.GATSBY_IMAGE_DOWNLOAD_DIR,
		imagesDownloadDir = Path.resolve(__dirname, "../../", downloadDir);

	if (!downloadDir) {
		console.error(new Error("Env Flag «GATSBY_IMAGE_DOWNLOAD_DIR» wasn’t found. Aborting image download …"));
		return null;
	}

	console.log(`Download takeshape images to ${ imagesDownloadDir } …`);

	// create dir if not exists
	if (!FileSystem.existsSync(imagesDownloadDir)) {
		mkdirp.sync(imagesDownloadDir);
	}

	let images,
		downloadedImagesCount = 0,
		skippedImages = 0;

	try {
		const data = await graphql(`
			query {
				sanity {
					allSanityImageAsset {
						url
						assetId
					}
				}
			}
		`).then(result => result.data.sanity);

		if (!data || !data.allSanityImageAsset) {
			console.log("No Images found. Hmmm…");
		}

		images = data.allSanityImageAsset;
		await Promise.all(images.map(async ({ url: path, assetId: id }) => {
			const
				extname = Path.extname(path),
				localPath = Path.resolve(__dirname, "../../", downloadDir, `${ id }${ extname.toLowerCase() }`);

			if (FileSystem.existsSync(localPath)) {
				console.log(`Skipped already downloaded image «${ localPath }»`);
				skippedImages++;
				return;
			}

			try {
				await downloadImage(path, localPath);
				downloadedImagesCount++;

				console.log(`Downloaded image to «${ localPath }»`);
			} catch (error) {
				console.log(`Error by downloading image «${ path }»`, error);
			}
		}));
	} catch (error) {
		console.error(error);
	}

	console.log(`Downloaded ${ downloadedImagesCount } images, ${ images.length - downloadedImagesCount - skippedImages } Errors, Skipped ${ skippedImages }. ${ images.length } total.`);
};

const download = async (data) => {
	try {
		await downloadAllImages(data);
	} catch (error) {
		console.log(error);
	}
};

module.exports = download;