import React from "react";
import Helmet from "react-helmet";
import PropTypes from "prop-types";
import Path from "path";
import { graphql, useStaticQuery } from "gatsby";

import BG_IMG_SRC from "../../sections/teaser/background.png";
import { FilesContext } from "../../../context-provider";


const Page = ({ title, description, children }) => {
	const query = useStaticQuery(graphql`
        query {
	        sanity {
            	generalData: allGeneralData (limit: 1) { 
	                siteName
	                domain
	                contact {
	                    address
	                    mail
	                    phone
	                    location {
	                        lat
	                        lng
	                    }
	                }
                }

                allSanityImageAsset {
                    url
                    assetId
                }
            }

            sitePage {
                path
            }
        }
	`);

	const files = query.sanity.allSanityImageAsset;
	const generalData = query.sanity.generalData[0];
	const url = Path.resolve(generalData.domain, query.sitePage.path);
	const documentTitle = `${ title } – ${ generalData.siteName }`;

	return (
		<React.Fragment>
			<Helmet>
				<title>{ documentTitle }</title>

				<meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />

				{ /* Basic */ }
				<meta name="description" content={ description } />

				{ /* SOCIAL */ }
				<meta property="og:title" content={ documentTitle } />
				<meta property="og:description" content={ description } />
				<meta property="og:url" content={ url } />
				<meta property="og:type" content="website" />
				<meta property="og:image" content={ `${ process.env.GATSBY_BASE_URL }${ BG_IMG_SRC }` } />

				{ /* BING Location */ }
				<meta name="geo.position" content={ `${ generalData.contact.location.lat }; ${ generalData.contact.location.lng }` } />
				<meta name="geo.placename" content={ "Fachhochschule Potsdam" } />
				<meta name="geo.region" content={ "DE" } />

				{ /* Developer Info */ }
				<meta name="author" content="Joseph Ribbe, Coderwelsch" />
			</Helmet>

			<FilesContext.Provider value={ files }>
				{ children }
			</FilesContext.Provider>
		</React.Fragment>
	);
};

Page.propTypes = {
	title: PropTypes.string.isRequired,
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.arrayOf(PropTypes.element)
	]),
};

export default Page;