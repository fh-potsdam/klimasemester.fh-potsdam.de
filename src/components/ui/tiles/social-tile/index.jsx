import React from "react";

import InstagramEmbed from "react-instagram-embed";

import Styles from "./index.module.scss";


const SocialTile = ({ url }) => {
	if (url.startsWith("https://www.instagram.com/")) {
		return (
			<InstagramEmbed
				url={ url }
				hideCaption={ true }
				maxWidth={ "100%" }
				className={ Styles.socialTile }
			/>
		)
	}

	return null;
};

export default SocialTile;