const slugify = require("slugify");
const path = require("path");

const downloadAllImages = require("./src/gatsby-nodes/download-all-images");


exports.createPages = async (data) => {
	await downloadAllImages(data);
	// await createSubpages(data);
};

async function createSubpages ({ graphql, actions }) {
	const { createPage } = actions;
	const subpageTemplate = path.resolve(`src/templates/subpage.jsx`);

	const { data } = await graphql(`
		query {
			ts: takeshape {
				sp: getSubpageList {
					items {
						title
						description
						contentHtml
					}
				}
			}
		}
	`);

	data.ts.sp.items.forEach(page => {
		createPage({
			path: `${ slugify(page.title.toLowerCase()) }`,
			component: subpageTemplate,
			context: {
				...page
			}
		});
	});
}