import { graphql, useStaticQuery } from "gatsby";
import React from "react";
import GatsbyImage from "gatsby-image";


function findFileById (id, filesList = []) {
	for (const file of filesList) {
		if (file.name === id) {
			return file;
		}
	}

	return null;
}


const Image = ({ id, ...props }) => {
	const { localFiles } = useStaticQuery(graphql`
        query {
            localFiles: allFile (filter: {extension: {eq: "jpg"}}) {
                nodes {
                    name
                    childImageSharp {
                        resize (width: 512) {
                            src
                        }

                        fluid (maxWidth: 600) {
	                        ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        }
	`);

	const image = findFileById(id, localFiles.nodes);

	return (
		<GatsbyImage
			{ ...props }
			key={ id }
			fluid={ image.childImageSharp.fluid }
		/>
	);
};

export default Image;