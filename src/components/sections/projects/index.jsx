import React, { Component } from "react";

import Section from "react-bulma-components/lib/components/section/section";

import AppStore from "../../../store/AppStore";
import Toolbar from "./toolbar";
import Tiles from "../../ui/tiles";


export default class Projects extends Component {
	state = {
		filterTags: []
	};

	onSelectionChanged (filterTags) {
		this.setState({
			...this.state,
			filterTags
		});
	}

	setTag (tag) {
		this.setState({
			...this.state,
			filterTags: [ tag ]
		});
	}
	
	render () {
		return (
			<Section className={ "has-padding-top-md" }>

				<Toolbar
					onSelectionChanged={ this.onSelectionChanged.bind(this) }
				/>

				<Tiles
					store={ AppStore }
					filter={ this.state.filterTags }
					onTagClick={ this.setTag.bind(this) }
				/>

			</Section>
		);
	}
}
