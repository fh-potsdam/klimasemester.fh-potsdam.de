import { graphql, useStaticQuery } from "gatsby";
import React from "react";
import Content from "react-bulma-components/lib/components/content/content";

import Hero from "react-bulma-components/lib/components/hero/hero";
import Body from "react-bulma-components/lib/components/hero/components/hero-body";
import Container from "react-bulma-components/lib/components/container/container";
import Heading from "react-bulma-components/lib/components/heading/heading";
import OpticalAlignment from "optical-aligned-text";
import BlockContent from "@sanity/block-content-to-react";

import LOGO_SRC from "../../../assets/logos/fh-potsdam.svg";
import Styles from "./index.module.scss";


const Teaser = () => {
	const query = useStaticQuery(graphql`
		query {
			sanity {
                allHome (limit: 1) {
	                teaser {
	                    headline
	                    subline
	                    contentRaw
	                }
                }
            }
		}
	`);

	const {
		headline,
		subline,
		contentRaw
	} = query.sanity.allHome[0].teaser;

	return (
		<React.Fragment>
			<Hero className={ Styles.hero }>
				<Body className={ "has-padding-md has-padding-top-xl" }>
					<a
						href={ "https://fh-potsdam.de" }
						target="_blank"
						rel="noopener noreferrer"
						className={ Styles.logoWrapper }>
						<img
							alt={ "Fachhochschule Potsdam" }
							src={ LOGO_SRC }
							className={ Styles.logo }
						/>
					</a>

					<Container>

						<Heading
							textAlignment={ "centered" }
							textColor={ "primary" }
							renderAs={ "h1" }
							textSize={ 1 }
							className={ Styles.fatHeading }>
							#klima&shy;semester
						</Heading>

						<div className={ Styles.contentWrapper }>
							<OpticalAlignment rules={ [{
								test: /U/,
								offset: -0.1,
							}] }>
								<React.Fragment>
									<Heading
										renderAs={ "h2" }
										textWeight={ "bold" }
										dangerouslySetInnerHTML={ { __html: headline } }
									/>

									<Heading
										subtitle
										renderAs={ "h3" }
										dangerouslySetInnerHTML={ { __html: subline } }
									/>

									<Content
										size={ "small" }
										renderAs={ "div" }>

										<BlockContent
											blocks={ contentRaw }
										/>

									</Content>

								</React.Fragment>
							</OpticalAlignment>
						</div>

					</Container>
				</Body>
			</Hero>

			<hr />
		</React.Fragment>
	);
};

export default Teaser;