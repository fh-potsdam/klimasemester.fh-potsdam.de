const ImageDownloader = require("image-downloader");


module.exports = function downloadImage (url, localPath) {
	return new Promise((resolve, reject) =>
		ImageDownloader.image({
			url: url,
			dest: localPath
		})
		.then(resolve)
		.catch(reject)
	);
};